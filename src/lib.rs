pub struct IntCode {
    pub code: Vec<i32>,
    orig: Vec<i32>,
    program_counter: usize,
    input: Vec<i32>,
    input_pos: usize,
}

impl IntCode {
    pub fn new(code: Vec<i32>) -> Self {
        Self {
            code: code.clone(),
            orig: code,
            program_counter: 0,
            input: Vec::new(),
            input_pos: 0,
        }
    }

    pub fn run_with_inputs(&mut self, input: Vec<i32>) {
        self.input_pos = 0;
        self.input = input;

        self.run();
    }

    pub fn reset(&mut self) {
        self.program_counter = 0;
        self.input_pos = 0;
        self.code = self.orig.clone();
    }

    // Return (opcode, params)
    // true -> parameter mode, false -> immediatmode
    fn fetch_opcode(&mut self) -> (i32, (bool, bool, bool)) {
        let v = self.fetch() as i32;
        let param3: bool = (v / 10000) == 0;
        let param2: bool = ((v % 10000) / 1000) == 0;
        let param1: bool = ((v % 1000) / 100) == 0;
        let opcode = v % 100;

        (opcode, (param1, param2, param3))
    }

    pub fn run(&mut self) {
        loop {
            let (opcode, modes) = self.fetch_opcode();
            match opcode {
                1 => self.add(modes),
                2 => self.mul(modes),
                3 => self.input(),
                4 => self.output(modes),
                5 => self.jump_if_true(modes),
                6 => self.jump_if_false(modes),
                7 => self.less_than(modes),
                8 => self.equals(modes),
                99 => break,
                _ => unimplemented!("Unimplemented opcode {}", opcode),
            }
        }
    }

    fn fetch(&mut self) -> i32 {
        let v: i32 = self.code[self.program_counter];
        self.program_counter += 1;
        v
    }

    fn add(&mut self, (mode1, mode2, _mode3): (bool, bool, bool)) {
        let a = self.access_value(mode1);
        let b = self.access_value(mode2);
        let dest = self.fetch() as usize;
        self.code[dest] = a + b;
    }

    fn mul(&mut self, (mode1, mode2, _mode3): (bool, bool, bool)) {
        let a = self.access_value(mode1);
        let b = self.access_value(mode2);
        let dest = self.fetch() as usize;
        self.code[dest] = a * b;
    }

    fn input(&mut self) {
        let dest = self.fetch() as usize;
        self.code[dest] = self.input[self.input_pos];
        self.input_pos += 1;
    }

    fn output(&mut self, (mode1, _mode2, _mode3): (bool, bool, bool)) {
        let v = self.access_value(mode1);
        println!("Output: {}", v);
    }

    fn jump_if_true(&mut self, (mode1, mode2, _mode3): (bool, bool, bool)) {
        let a = self.access_value(mode1);
        let b = self.access_value(mode2);
        if a != 0 {
            self.program_counter = b as usize;
        }
    }

    fn jump_if_false(&mut self, (mode1, mode2, _mode3): (bool, bool, bool)) {
        let a = self.access_value(mode1);
        let b = self.access_value(mode2);
        if a == 0 {
            self.program_counter = b as usize;
        }
    }

    fn less_than(&mut self, (mode1, mode2, _mode3): (bool, bool, bool)) {
        let a = self.access_value(mode1);
        let b = self.access_value(mode2);
        let dest = self.fetch() as usize;
        self.code[dest] = (a < b) as i32;
    }

    fn equals(&mut self, (mode1, mode2, _mode3): (bool, bool, bool)) {
        let a = self.access_value(mode1);
        let b = self.access_value(mode2);
        let dest = self.fetch() as usize;
        self.code[dest] = (a == b) as i32;
    }

    fn access_value(&mut self, parameter_mode: bool) -> i32 {
        let v = self.fetch();
        if parameter_mode {
            self.code[v as usize]
        } else {
            v
        }
    }
}

impl From<String> for IntCode {
    fn from(input: String) -> Self {
        Self::new(
            input
                .trim()
                .split(",")
                .map(|x| x.parse::<i32>())
                .filter_map(Result::ok)
                .collect(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! assert_ic_run {
        ($in:expr, $out:expr) => {{
            let mut intcode = IntCode::new($in);
            intcode.run_with_inputs(vec![1]);
            assert_eq!(intcode.code, $out)
        }};
    }

    #[test]
    fn test_given_values() {
        assert_ic_run!(vec![1, 0, 0, 0, 99], [2, 0, 0, 0, 99]);
        assert_ic_run!(vec![2, 3, 0, 3, 99], [2, 3, 0, 6, 99]);
        assert_ic_run!(vec![2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801]);
        assert_ic_run!(
            vec![1, 1, 1, 4, 99, 5, 6, 0, 99],
            [30, 1, 1, 4, 2, 5, 6, 0, 99]
        );
    }

    #[test]
    fn test_some_more() {
        assert_ic_run!(
            vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8],
            [3, 9, 8, 9, 10, 9, 4, 9, 99, 0, 8]
        )
    }

    #[test]
    fn test_add() {
        assert_ic_run!(vec![1, 1, 2, 5, 99, 0], [1, 1, 2, 5, 99, 3])
    }

    #[test]
    fn test_mul() {
        assert_ic_run!(vec![2, 1, 2, 5, 99, 0], [2, 1, 2, 5, 99, 2])
    }

    #[test]
    fn test_input() {
        assert_ic_run!(vec![3, 0, 4, 0, 99], [1, 0, 4, 0, 99]);
    }
}
