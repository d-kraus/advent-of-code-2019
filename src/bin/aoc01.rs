use std::io::{BufRead, BufReader};
use std::fs::File;

fn main() {
    let file = File::open("input/aoc1").expect("File not found");
    let vec: Vec<u32> = BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .map(|x| x.parse::<u32>())
        .map(|x| x.unwrap())
        .collect();
    let result: u32 = calculate_fuel(vec);
    println!("Result is {}", result);
}

fn calc(i:u32)-> u32 {
    match i {
        0..=5 => 0,
        _ => i / 3 -2
    }
}

fn calculate_fuel_for_module(part: u32) -> u32 {
    let mut total:u32 = 0;
    let mut extra:u32 = part;
    loop{
        if extra == 0 {
            break;
        }
        extra = calc(extra);
        total = total + extra;
    }
    total
}

fn calculate_fuel(modules: Vec<u32>) -> u32 {
    modules
        .into_iter()
        .map(|x| calculate_fuel_for_module(x))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calculate_fuel_for_module() {
        assert_eq!(calculate_fuel_for_module(5), 0);
        assert_eq!(calculate_fuel_for_module(12), 2);
        assert_eq!(calculate_fuel_for_module(14), 2);
        assert_eq!(calculate_fuel_for_module(1969), 966);
        assert_eq!(calculate_fuel_for_module(100756), 50346);
    }

    #[test]
    fn test_calculate_fuel() {
        let vec:Vec<u32> = vec![12, 14, 1969];
        assert_eq!(calculate_fuel(vec), 970);
    }
}
