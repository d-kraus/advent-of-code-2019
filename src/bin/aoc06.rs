use std::collections::{BTreeMap, HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::FromIterator;

#[derive(Debug)]
struct System {
    objects: Vec<Object>,
    index: HashMap<String, usize>,
}

impl System {
    fn total_orbits(&self) -> u32 {
        self.objects.iter().map(|x| self.count_orbits(&x)).sum()
    }

    fn count_orbits(&self, object: &Object) -> u32 {
        let mut count = 1;
        let mut current: &Object = object;
        while let Some(id) = self.get_parent(current) {
            count += 1;
            current = &self.objects[id];
        }

        count
    }

    fn get_parent(&self, o: &Object) -> Option<usize> {
        match self.index.get(&o.orbits) {
            Some(pos) => Some(*pos),
            None => None,
        }
    }

    fn get_nodes(&self, object: &Object) -> HashSet<String> {
        let mut nodes: HashSet<String> = HashSet::new();
        let mut current: &Object = object;
        while let Some(id) = self.get_parent(current) {
            nodes.insert(current.id.clone());
            current = &self.objects[id];
        }
        nodes
    }
    fn from_to(&self, from: String, to: String) -> u32 {
        // 1) get list of nodes between point and COM
        // 2) sort nodes in that list by distance to COM
        // 3) distance between from and to is:
        // dist(from) + dist(to) - dist(max_node) - 2

        let you: &Object = &self.objects[*self.index.get(&from).unwrap()];
        let santa: &Object = &self.objects[*self.index.get(&to).unwrap()];
        let nodes_you = self.get_nodes(you);
        let nodes_santa = self.get_nodes(santa);

        let dist: u32 = *nodes_you
            .intersection(&nodes_santa)
            .map(|x| self.index[x])
            .map(|x| &self.objects[x])
            .map(|x| (self.count_orbits(x), x.id.clone()))
            .collect::<BTreeMap<u32, String>>()
            .iter()
            .next_back()
            .unwrap()
            .0;

        self.count_orbits(you) + self.count_orbits(santa) - 2 * dist.clone() - 2
    }
}

impl FromIterator<String> for System {
    fn from_iter<I: IntoIterator<Item = String>>(iter: I) -> Self {
        let mut vec: Vec<Object> = Vec::new();
        for i in iter {
            let names: Vec<&str> = i.split(')').collect();
            let id: String = names[1].to_string();
            let orbits: String = names[0].to_string();
            vec.push(Object {
                id: id,
                orbits: orbits,
            });
        }

        println!("{}", vec.len());
        let mut index: HashMap<String, usize> = HashMap::with_capacity(vec.len());
        for (i, item) in vec.iter().enumerate() {
            index.insert(item.id.clone(), i);
        }

        Self {
            objects: vec,
            index: index,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Object {
    id: String,
    orbits: String,
}

fn main() {
    let f = File::open("input/aoc6").expect("Something went wrong reading the file");
    let file = BufReader::new(&f);

    let system: System = file.lines().map(|x| x.unwrap()).collect();

    println!("Part 1, total orbits: {}", system.total_orbits());
    println!(
        "Part 2, transfers: {}",
        system.from_to("YOU".to_string(), "SAN".to_string())
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let vec: Vec<Object> = vec![Object {
            id: "AAA".to_string(),
            orbits: "Com".to_string(),
        }];
        let mut hm: HashMap<String, usize> = HashMap::new();
        hm.insert("AAA".to_string(), 0);
        let system: System = System {
            objects: vec,
            index: hm,
        };
        assert_eq!(system.total_orbits(), 1);
    }
}
