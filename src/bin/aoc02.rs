use aoc2019::IntCode;
use std::fs;

fn main() {
    let content: String =
        fs::read_to_string("input/aoc2").expect("Something went wrong reading the file");
    let mut intcode: IntCode = content.clone().into();
    println!("Result for part 1 is {:?}", intcode.run());

    println!("Result for part 2 is {}", (brute_force(content).unwrap()));
}

fn brute_force(content: String) -> Option<i32> {
    'outer: for noun in 0..99 {
        for verb in 0..99 {
            let mut intcode: IntCode = content.clone().into();
            intcode.code[1] = noun;
            intcode.code[2] = verb;
            intcode.run();
            if intcode.code[0] == 19690720 {
                return Some(100 * noun + verb);
            }
        }
    }
    None
}
