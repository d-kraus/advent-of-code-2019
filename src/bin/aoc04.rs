use itertools::Itertools;

fn main() {
    let mut counter = 0;
    //Input from advent of code 109165-576723
    for pw in 109165..576724 {
        if never_decreases(pw) && adjacent_digits_equal(pw) {
            counter += 1;
        }
    }

    println!("Result for part1: {}", counter);

    counter = 0;
    for pw in 109165..576724 {
        if never_decreases(pw) && adjacent_digits_equal(pw) && part2(pw) {
            counter += 1;
        }
    }

    println!("Result for part2: {}", counter);
}

fn never_decreases(v: u32) -> bool {
    v.to_string()
        .chars()
        .map(|d| d.to_digit(10).unwrap())
        .collect::<Vec<u32>>()
        .windows(2)
        .all(|w| w[0] <= w[1])
}

fn adjacent_digits_equal(v: u32) -> bool {
    v.to_string()
        .chars()
        .map(|d| d.to_digit(10).unwrap())
        .collect::<Vec<u32>>()
        .windows(2)
        .any(|w| w[0] == w[1])
}

fn part2(v: u32) -> bool {
    v.to_string()
        .chars()
        .map(|c| (c, 1))
        .coalesce(|(c, n), (d, m)| {
            if c == d {
                Ok((c, n + m))
            } else {
                Err(((c, n), (d, m)))
            }
        })
        .any(|(_c, n)| n == 2)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_non_decreasing() {
        assert_eq!(never_decreases(111111), true);
        assert_eq!(never_decreases(223450), false);
        assert_eq!(never_decreases(223456), true);
    }

    #[test]
    fn test_2_adjadent_equal() {
        assert_eq!(adjacent_digits_equal(111111), true);
        assert_eq!(adjacent_digits_equal(123450), false);
        assert_eq!(adjacent_digits_equal(223456), true);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(112233), true);
        assert_eq!(part2(123444), false);
        assert_eq!(part2(111122), true);
    }
}
