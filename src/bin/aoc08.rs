use std::fs;

struct Image {
    height: usize,
    width: usize,
    pixels: Vec<u32>,
}

impl From<(u32, u32, String)> for Image {
    fn from((width, height, raw_pixel): (u32, u32, String)) -> Self {
        let v: Vec<u32> = raw_pixel.chars().filter_map(|x| x.to_digit(10)).collect();

        Self {
            height: height as usize,
            width: width as usize,
            pixels: v,
        }
    }
}

impl Image {
    fn verify(&self) -> u32 {
        // Find the slice with smallest number of 0
        let slice = self
            .pixels
            .chunks(self.width * self.height)
            .min_by_key(|x| count_digit(x, 0))
            .unwrap();

        let num_1 = count_digit(slice, 1);
        let num_2 = count_digit(slice, 2);
        num_1 * num_2
    }

    // 0 - black
    // 1 - white
    // 2 -transparent
    fn get_pixel_value(&self, mut pos: usize) -> u32 {
        let mut v = 2;
        while v == 2 {
            v = self.pixels[pos];
            pos = pos + self.width * self.height;
        }
        v
    }

    fn draw(&self) {
        let pixel_values: Vec<u32> = (0..self.width * self.height + 1)
            .map(|x| self.get_pixel_value(x))
            .collect();

        for line in pixel_values.chunks(self.width) {
            let output: String = line
                .iter()
                .map(|&x| std::char::from_digit(x, 10).unwrap())
                .collect();

            println!("{}", output);
        }
    }
}

fn count_digit(slice: &[u32], digit: u32) -> u32 {
    let mut count = 0;
    for d in slice {
        if *d == digit {
            count += 1;
        }
    }
    count
}

fn main() {
    let content: String =
        fs::read_to_string("input/aoc8").expect("Something went wrong reading the file");
    let img: Image = (25, 6, content).into();
    println!("Part 1 corruption check returnes: {}", img.verify());

    println!("Part 2:");
    img.draw();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = "123456789012".to_string();
        let img: Image = (3, 2, input).into();

        assert_eq!(img.height, 2);
        assert_eq!(img.width, 3);
        assert_eq!(img.pixels, vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2]);
        assert_eq!(img.verify(), 1);
    }

    #[test]
    fn test_2() {
        let input = "1010101200001122".to_string();
        let img: Image = (8, 2, input).into();

        assert_eq!(img.verify(), 18);
    }
}
