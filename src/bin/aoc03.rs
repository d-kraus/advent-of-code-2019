use std::collections::hash_map::Entry;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let f = File::open("input/aoc3").expect("Can not read input file");
    let f = BufReader::new(f);
    let mut lines = f.lines();

    let path1 = Path::from(lines.next().unwrap().unwrap());
    let path2 = Path::from(lines.next().unwrap().unwrap());

    let mut res_part_1 = path1
        .intersect(&path2)
        .iter()
        .map(|x| x.dist())
        .collect::<Vec<u32>>();
    res_part_1.sort();
    println!("Solution for first part: {:?}", res_part_1.first().unwrap());

    println!(
        "Solution for second part: {:?}",
        shortest_steps(path1, path2)
    );
}

fn shortest_steps(path1: Path, path2: Path) -> u32 {
    let mut res = Vec::new();
    for point in path1.intersect(&path2) {
        res.push(path1.get(point) + path2.get(point));
    }
    res.sort();
    *res.first().unwrap()
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, PartialEq)]
struct Instruction {
    length: u32,
    direction: Direction,
}

impl Instruction {
    fn new(length: u32, dir: Direction) -> Self {
        Self {
            length: length,
            direction: dir,
        }
    }
}

fn parse_instruction(raw: &str) -> Instruction {
    // R998
    let mut raw_chars = raw.chars();
    let dir: Direction = match raw_chars.next() {
        Some('U') => Direction::Up,
        Some('D') => Direction::Down,
        Some('L') => Direction::Left,
        Some('R') => Direction::Right,
        _ => panic!("Invalid Direction"),
    };
    let length: u32 = raw_chars.collect::<String>().parse().unwrap();
    Instruction::new(length, dir)
}

fn parse_line(line: String) -> Vec<Instruction> {
    // R998,U367
    line.split(',')
        .map(|x| parse_instruction(x))
        .collect::<Vec<Instruction>>()
}

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone, PartialOrd, Ord)]
struct Point(i32, i32);

impl Point {
    fn mv(&self, dir: Direction) -> Point {
        match dir {
            Direction::Up => Point(self.0 + 1, self.1),
            Direction::Down => Point(self.0 - 1, self.1),
            Direction::Left => Point(self.0, self.1 - 1),
            Direction::Right => Point(self.0, self.1 + 1),
        }
    }

    fn dist(&self) -> u32 {
        (self.0.abs() + self.1.abs()) as u32
    }
}

struct Path {
    path: HashMap<Point, u32>,
    pos: Point,
    steps: u32,
}

impl Path {
    fn new() -> Self {
        Self {
            pos: Point(0, 0),
            steps: 0,
            path: HashMap::new(),
        }
    }

    fn get(&self, key: Point) -> u32 {
        *self.path.get(&key).unwrap()
    }

    fn walk(&mut self, instructions: Vec<Instruction>) {
        for instr in instructions {
            for _i in 0..instr.length {
                self.pos = self.pos.mv(instr.direction);
                self.steps += 1;

                match self.path.entry(self.pos) {
                    Entry::Vacant(v) => {
                        v.insert(self.steps);
                    }
                    _ => {}
                }
            }
        }
    }

    fn intersect(&self, other: &Path) -> Vec<Point> {
        let keys1: HashSet<Point> = self.path.keys().into_iter().cloned().collect();
        let keys2: HashSet<Point> = other.path.keys().into_iter().cloned().collect();

        keys1.intersection(&keys2).into_iter().cloned().collect()
    }
}

impl From<String> for Path {
    fn from(raw: String) -> Self {
        let instr: Vec<Instruction> = parse_line(raw);
        let mut path = Self::new();
        path.walk(instr);
        path
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn path_intersect() {
        let path1 = Path::from("U1,R2".to_string());
        let path2 = Path::from("U1,R1,D1".to_string());

        let mut result = path1.intersect(&path2);
        result.sort();
        assert_eq!(result, [Point(1, 0), Point(1, 1)]);
    }

    #[test]
    fn test_simple_path() {
        let path = Path::from("U1,R2".to_string());

        assert_eq!(path.path.contains_key(&Point(1, 0)), true);
        assert_eq!(path.path.contains_key(&Point(1, 1)), true);
        assert_eq!(path.path.contains_key(&Point(1, 2)), true);
    }

    #[test]
    fn test_test_simple_steps() {
        let path = Path::from("U1,R2".to_string());

        assert_eq!(path.path.get(&Point(1, 0)), Some(&1));
        assert_eq!(path.path.get(&Point(1, 1)), Some(&2));
        assert_eq!(path.path.get(&Point(1, 2)), Some(&3));
    }

    #[test]
    fn test_test_steps() {
        let path = Path::from("U1,R3,D1,L1,U2".to_string());

        assert_eq!(path.path.get(&Point(1, 3)), Some(&4));
        assert_eq!(path.path.get(&Point(0, 2)), Some(&6));
        assert_eq!(path.path.get(&Point(1, 2)), Some(&3));
        assert_eq!(path.path.get(&Point(2, 2)), Some(&8));
    }

    #[test]
    fn test_test_steps_complex() {
        let path1 = Path::from("R75,D30,R83,U83,L12,D49,R71,U7,L72".to_string());
        let path2 = Path::from("U62,R66,U55,R34,D71,R55,D58,R83".to_string());

        assert_eq!(shortest_steps(path1, path2), 610);
    }

    #[test]
    fn test_test_steps_complex2() {
        let path1 = Path::from("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51".to_string());
        let path2 = Path::from("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7".to_string());

        assert_eq!(shortest_steps(path1, path2), 410);
    }
    #[test]
    fn test_parse_line() {
        let input: String = "U7,R6,D4,L4".to_string();
        let expected: Vec<Instruction> = vec![
            Instruction::new(7, Direction::Up),
            Instruction::new(6, Direction::Right),
            Instruction::new(4, Direction::Down),
            Instruction::new(4, Direction::Left),
        ];
        assert_eq!(parse_line(input), expected);
    }

    #[test]
    fn test_point_mv() {
        let mut point = Point(0, 0).mv(Direction::Up);
        assert_eq!(point, Point(1, 0));
        point = point.mv(Direction::Left);
        assert_eq!(point, Point(1, -1));
        point = point.mv(Direction::Down).mv(Direction::Down);
        assert_eq!(point, Point(-1, -1));
        point = point.mv(Direction::Up).mv(Direction::Right);
        assert_eq!(point, Point(0, 0));
    }
}
