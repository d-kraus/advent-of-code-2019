use aoc2019::IntCode;
use std::fs;

fn main() {
    let content: String =
        fs::read_to_string("input/aoc5").expect("Something went wrong reading the file");
    let mut intcode: IntCode = content.into();
    intcode.run_with_inputs(vec![1]);
    intcode.reset();
    intcode.run_with_inputs(vec![5]);
}
